/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import * as express from 'express';

import * as db from './database';
import { model } from './utils/news.model';
import { crudControllers } from './utils/news.controllers';

const app = express();
const { getAll, createOne, updateOne, removeOne } = crudControllers(model)

// only works with express 4 or above
app.use(express.json());

app.get('/api/news', getAll);
app.post('/api/news', createOne);
app.put('/api/news', updateOne);
app.delete('/api/news', removeOne);

export { app };

const port = process.env.port || 3333;
const server = app.listen(port, async () => {
  await db.connect();
  console.log(`Listening at http://localhost:${port}/api/news`);
});
server.on('error', console.error);
