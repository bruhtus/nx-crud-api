function crudControllers(model) {
  // read all items
  async function getAll(req, res) {
    const docs = await model.find();
    return res.status(200).json({ data: docs });
  }

  // create one item
  async function createOne(req, res) {
    const doc = await model.create(req.body);
    return res.status(201).json(req.body);
  };

  // update one item based on the title
  async function updateOne(req, res) {
    const updated = await model.findOneAndUpdate(
      { title: req.body.title },
      { title: req.body.newTitle },
      { new: true },
    );

    return res.status(201).json({ data: updated });
  };

  // delete one item
  async function removeOne(req, res) {
    const removed = await model.findOneAndRemove({
      title: req.body.title
    });

    return res.status(201).json({ data: removed });
  };

  return { getAll, createOne, updateOne, removeOne };
}

export { crudControllers };
