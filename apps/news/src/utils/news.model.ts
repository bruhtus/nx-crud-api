import * as mongoose from 'mongoose';

interface News {
  title: string;
  content: string;
}

const schema = new mongoose.Schema<News>({
  title: {
    type: String,
    required: true,
  },
  content: String,
});

const model = mongoose.model<News>('news', schema);

export { model };
