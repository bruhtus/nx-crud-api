import * as supertest from 'supertest';

import * as db from '../../database'
import { app } from '../../main';

beforeAll(async () => await db.connect());
afterAll(async () => await db.closeDatabase());
afterAll(async () => await db.clearDatabase());

describe('crud routes exist', () => {
  test('can write news', async () => {
    const res = await supertest(app)
    .post('/api/news')
    .send({ title: 'jakarta banjir', content: 'masih tenggelam' });

    expect(res.statusCode).toBe(201);
    expect(res.body).toMatchObject({
      title: 'jakarta banjir',
      content: 'masih tenggelam'
    });
  });

  test('can read news', async () => {
    const res = await supertest(app)
    .get('/api/news');

    expect(res.statusCode).toBe(200);
    expect(res.body.data).toMatchObject([{
      title: 'jakarta banjir',
      content: 'masih tenggelam'
    }]);
  });

  test('can update news', async () => {
    const res = await supertest(app)
    .put('/api/news')
    .send({ title: 'jakarta banjir', newTitle: 'jakarta tenggelam' });

    expect(res.statusCode).toBe(201);
    expect(res.body.data).toMatchObject({
      title: 'jakarta tenggelam'
    });
  });

  test('can delete news', async () => {
    const res = await supertest(app)
    .delete('/api/news')
    .send({ title: 'jakarta tenggelam' })
    .set('title', 'jakarta tenggelam' );

    expect(res.statusCode).toBe(201);
    expect(res.body.data).toMatchObject({
      title: 'jakarta tenggelam'
    });
  });
});
