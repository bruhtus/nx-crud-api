# NxCrudApi

This project was generated using [Nx](https://nx.dev).

## Requirements

- docker
- node
- yarn

## Setup

- run the docker command (for mongodb database):
```sh
docker run --name test-mongo -dit -p 27017:27017 --rm mongo:4.4.1
```

## References

- [Use yarn as default package manager in Nx](https://github.com/nrwl/nx/issues/4513#issuecomment-759708345).
- [Import supertest](https://tutorialedge.net/typescript/testing-typescript-api-with-jest/).
- [Built-in body parser in express](https://dev.to/taylorbeeston/you-probably-don-t-need-body-parser-in-your-express-apps-3nio).
- [Mongoose with typescript](https://mongoosejs.com/docs/typescript.html).
